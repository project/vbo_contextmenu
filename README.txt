
VBO Context Menu
------------------
Provides a context menu for Views Bulk Operations.

Installation
------------
Unzip the module into your modules directory.
Download the jQuery Context Menu plugin at http://abeautifulsite.net/notebook/80
If you have the libraries module, extract the archive content and place the jquery.contextMenu directory in sites/all/libraries (create it if it does not exist).
Otherwise, place it directly in the module folder.
Enable the module.
Create or edit a VBO view and go to the style option form (click on the icon next to "Style: Bulk Operations").
Set the options at the bottom of the form.

Maintainers
-----------
Julien De Luca